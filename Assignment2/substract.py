import cv2
import numpy as np

image1 = np.asarray(cv2.imread("tests/picachu.png"))
image2 = np.asarray(cv2.imread("report/3/result.png"))

diff1 = cv2.subtract(image2, image1)

cv2.imwrite("different2.png", diff1)
