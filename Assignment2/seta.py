import numpy as np
import cv2
import random

k = 2000
n = 12

max_diff = 200540161
epoches = 100

color_mutation_probability = 70
position_mutation_probability = 80

max_color = 16
max_pos = 64

color_shift = 256 / max_color
pos_shift = 8
rad = 16

color_genes = []
for i in range(max_color):
    color_genes.append(i)

position_genes = []
for i in range(max_pos):
    position_genes.append(i)

img1 = cv2.imread('tests/telegram.png', 1)


def populate():
    population = []
    for i in range(n):
        pic = []
        for j in range(k):
            pic.append([random.choice(position_genes), random.choice(position_genes),
                        random.choice(color_genes), random.choice(color_genes), random.choice(color_genes)])
        population.append(pic)
    return population


def sum_diff(m1, m2):
    sum = 0;
    for i in range(512):
        for j in range(512):
            sum = sum + max(m1[i][j], m2[i][j])
    return sum


def difference(image1, image2):
    diff1 = cv2.subtract(image1, image2)
    diff2 = cv2.subtract(image2, image1)
    b1, g1, r1 = cv2.split(diff1)
    b2, g2, r2 = cv2.split(diff2)
    return sum_diff(b1, b2) + sum_diff(g1, g2) + sum_diff(r1, r2)


def fitness(population):
    fitness_res = []

    for i in range(n):
        img2 = np.zeros((512, 512, 3), np.uint8)
        for j in range(k):
            cv2.circle(img2, (rad + population[i][j][0] * pos_shift, rad + population[i][j][1] * pos_shift), rad,
                       (population[i][j][2] * color_shift, population[i][j][3] * color_shift,
                        population[i][j][4] * color_shift), -1)

        fitness_res.append(difference(img1, img2))

        print(fitness_res[i], "Fitness: ", (1 - (fitness_res[i] / 200540160)) * 100)
    return fitness_res


def select_parents(fitness_res):
    parents = [max_diff, max_diff, max_diff, max_diff]
    parents_i = [0, 0, 0, 0]
    for i in range(n):
        if fitness_res[i] < parents[3]:
            if fitness_res[i] < parents[2]:
                parents[3] = parents[2]
                parents_i[3] = parents_i[2]
                if fitness_res[i] < parents[1]:
                    parents[2] = parents[1]
                    parents_i[2] = parents_i[1]
                    if fitness_res[i] < parents[0]:
                        parents[1] = parents[0]
                        parents_i[1] = parents_i[0]
                        parents[0] = fitness_res[i]
                        parents_i[0] = i
                    else:
                        parents[1] = fitness_res[i]
                        parents_i[1] = i
                else:
                    parents[2] = fitness_res[i]
                    parents_i[2] = i
            else:
                parents[3] = fitness_res[i]
                parents_i[3] = i
    print(parents)
    return parents_i


def create_child(c_pos, c_col1, c_col2, parent1, parent2):
    child = []
    for i in range(k):
        child_gen = []
        r = random.randint(1, 3)
        if r == 1:
            child_gen = parent1[i].copy()
        else:
            if r == 2:
                child_gen = parent2[i].copy()
            else:
                child_gen = parent2[i].copy()
                child_gen[c_pos] = parent1[i][c_pos]
            child_gen[c_col1] = parent1[i][c_col1]
            child_gen[c_col2] = parent1[i][c_col2]
        child.append(child_gen)
    return child


def crossover(population, parents_i):
    choice_pos = random.randint(0, 1)
    choice_col1 = random.randint(2, 4)
    choice_col2 = random.randint(2, 4)

    population_new = []
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[0]], population[parents_i[0]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[1]], population[parents_i[1]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[0]], population[parents_i[1]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[1]], population[parents_i[0]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[0]], population[parents_i[2]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[2]], population[parents_i[0]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[1]], population[parents_i[2]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[2]], population[parents_i[1]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[0]], population[parents_i[3]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[3]], population[parents_i[0]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[1]], population[parents_i[3]]))
    population_new.append(
        create_child(choice_pos, choice_col1, choice_col2, population[parents_i[3]], population[parents_i[1]]))
    return population_new


def mutation(population):
    for i in range(n):
        m = 10
        if random.randint(0, 100) < 50:
            m = 50
            if random.randint(0, 100) < 20:
                m = 200
                if random.randint(0, 100) < 10:
                    m = 500
            rng = random.randint(0, 50)
            for j in range(rng):
                t = random.randint(0, k)
                if t != k:
                    for p in range(2):
                        if random.randint(0, 100) <= position_mutation_probability:
                            pos_mutation = population[i][t][p]
                            pos_mutation = pos_mutation + random.randint(-9, 9)
                            if pos_mutation < 0:
                                pos_mutation = max_pos + pos_mutation
                            else:
                                if pos_mutation > max_pos:
                                    pos_mutation = pos_mutation - max_pos
                            population[i][t][p] = pos_mutation

                    for c in range(3):
                        if random.randint(0, 100) <= color_mutation_probability:
                            color_mutation = population[i][t][1 + c]
                            color_mutation = color_mutation + random.randint(-11, 11)
                            if color_mutation < 0:
                                color_mutation = max_color + color_mutation
                            else:
                                if color_mutation > max_color:
                                    color_mutation = color_mutation - max_color
                            population[i][t][1 + c] = color_mutation
    return population


best_res = max_diff
best_res_genes = []

population = populate()

for t in range(epoches):
    print("Epoches:", t)
    fitness_res = fitness(population)
    for i in range(n):
        if fitness_res[i] < best_res:
            best_res = fitness_res[i]
            best_res_genes = population[i]
            f = open('gen.txt', 'w')
            for iteml in best_res_genes:
                for item in iteml:
                    f.write("%s " % item)
                f.write("\n")
            f.close()

    parents_i = select_parents(fitness_res)
    population = crossover(population, parents_i)
    population = mutation(population)

print("BEST: ", best_res)

img = np.zeros((512, 512, 3), np.uint8)
for i in range(k):
    cv2.circle(img, (rad + best_res_genes[i][0] * pos_shift, rad + best_res_genes[i][1] * pos_shift), rad,
               (best_res_genes[i][2] * color_shift, best_res_genes[i][3] * color_shift,
                best_res_genes[i][4] * color_shift), -1)
cv2.imwrite('report/3/result.png', img)

res = np.zeros((512, 512, 3), np.uint8)
for i in range(k):
    cv2.circle(res, (rad + best_res_genes[i][0] * pos_shift, rad + best_res_genes[i][1] * pos_shift), rad,
               (best_res_genes[i][2] * color_shift, best_res_genes[i][3] * color_shift,
                best_res_genes[i][4] * color_shift), -1)

cv2.imshow('image', res)
# cv2.imwrite('res75.png',res)
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imshow('image', cv2.subtract(img1, res))
cv2.waitKey(0)
cv2.destroyAllWindows()
cv2.imshow('image', cv2.subtract(res, img1))
cv2.waitKey(0)
cv2.destroyAllWindows()
