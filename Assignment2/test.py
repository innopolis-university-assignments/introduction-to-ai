from random import randrange, randint, sample

import cv2
import numpy as np
from matplotlib import pyplot as plt

size_photo = 512  # Photo side's size
max_color = 255  # Each component of BGR may be in [0, 255]
min_radius = 5
max_radius = 10  # The radios of the circle
number_circles = 2000  # The number of circles on the member's image of population
number_generations = 100000  # The number of generations
number_populations = 8  # The number of population
number_parents = number_populations // 2  # The number of good selected members which will give children
path = "result.png"
colors = {(205, 205, 205), (40, 40, 40), (130, 130, 130), (164, 164, 164), (172, 172, 172)}
FLAG = True


class Gen:
    def __init__(self, center_coordinates, color, radius):
        self.center_coordinates = center_coordinates
        self.color = color
        self.radius = radius


class Chromosome:
    """
    This class represents chromosome
    Constructor: creates random image of 512x512 size

    """

    def __init__(self):
        self.score = 0
        self.gen_list = []
        rand_matrix = np.full((512, 512, 3), 255, np.uint8)
        for _ in range(number_circles):
            center_coordinates = (randrange(0, size_photo), randrange(0, size_photo))
            if FLAG:
                color = (randrange(0, max_color), randrange(0, max_color), randrange(0, max_color))
            else:
                color = sample(colors, 1)[0]
            radius = randrange(min_radius, max_radius)
            thickness = -1
            self.gen_list.append(Gen(center_coordinates, color, radius))
            rand_matrix = cv2.circle(rand_matrix, center_coordinates, radius, color, thickness)
        self.image = rand_matrix

    def display(self):

        cv2.imwrite(path, self.image)
        # plt.axis("off")
        # plt.imshow(cv2.cvtColor(self.image, cv2.COLOR_BGR2RGB))
        # plt.show()


class Population:
    """
        This class represents population
        Constructor: creates chromosomes

    """

    def __init__(self):
        self.population = []
        for _ in range(number_populations):
            self.population.append(Chromosome())

    @staticmethod
    def _mse(imageA, imageB):
        err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
        # err /= float(imageA.shape[0] * imageA.shape[1])
        return err

    def compare_images(self, imageB):
        m = self._mse(goal, imageB)  # [0,inf], where 0 means the same
        return m

    def fitness(self):
        for mem in range(number_populations):
            self.population[mem].score = self.compare_images(self.population[mem].image)

    def crossover(self):  # gen_list[0] - center gen_list[1] - color gen_list[2] - radius

        for i in range(-1, number_parents - 1, 1):
            one = []
            two = []
            n = number_circles // 2
            for j in range(n):
                coord_one = self.population[i].gen_list[j].center_coordinates
                coord_two = self.population[i + 1].gen_list[j + n].center_coordinates

                color_one = self.population[i].gen_list[j].color
                color_two = self.population[i + 1].gen_list[j + n].color

                rad_one = self.population[i].gen_list[j].radius
                rad_two = self.population[i + 1].gen_list[j + n].radius
                one.append(Gen(coord_one, color_one, rad_one))
                two.append(Gen(coord_two, color_two, rad_two))

            self.population[i+number_parents + 1].gen_list = one + two

    def mutation(self):  # gen_list[0] - center gen_list[1] - color gen_list[2] - radius
        for rand_child in range(number_parents, number_populations-1):
            rand_mutation = randint(15, 25)
            for i in range(rand_mutation):
                rand_gen = randint(1, number_circles - 1)
                rand_gen_type = randint(0, 1)
                x = self.population[rand_child].gen_list[rand_gen]
                if rand_gen_type == 0:
                    a = (x.center_coordinates[0] + randint(-max_radius, max_radius))  # % max_radius
                    b = (x.center_coordinates[1] + randint(-max_radius, max_radius))
                    self.population[rand_child].gen_list[rand_gen].center_coordinates = (a, b)
                elif rand_gen_type == 1:
                    if FLAG:
                        a = (x.color[0] + randint(-max_color, max_color))  # % max_color
                        b = (x.color[1] + randint(-max_color, max_color))
                        c = (x.color[2] + randint(-max_color, max_color))
                        self.population[rand_child].gen_list[rand_gen].color = (a, b, c)
                    else:
                        color = sample(colors, 1)[0]
                        self.population[rand_child].gen_list[rand_gen].color = color  # (a, b, c)

                else:
                    radius = abs((x.radius + randint(-min_radius, min_radius)))  # % max_radius
                    self.population[rand_child].gen_list[rand_gen].radius = radius

            temp_chromosome = np.full((512, 512, 3), 255, np.uint8)
            for i in range(number_circles):
                center_coordinates = self.population[rand_child].gen_list[i].center_coordinates
                color = self.population[rand_child].gen_list[i].color
                radius = self.population[rand_child].gen_list[i].radius
                thickness = -1
                temp_chromosome = cv2.circle(temp_chromosome, center_coordinates, radius, color, thickness)
            self.population[rand_child].image = temp_chromosome

    def selection(self):
        self.population = sorted(self.population, key=lambda x: x.score, reverse=False)
        self.crossover()
        self.mutation()


goal = np.asarray(cv2.imread("tests/monalisa.png"))
population = Population()

x = []
print("start")
for K in range(number_generations):
    population.fitness()
    population.selection()
    x.append(population.population[0].score)
    if K % 1000 == 0:
        print("Generation: " + str(K) + " Score: " + str(population.population[0].score))
        population.population[0].display()

# population.population[0].display()
